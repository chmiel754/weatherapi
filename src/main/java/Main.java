import org.json.JSONException;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static String KEY = "4960f2d5f56b4d8b881204839202001";

    public static void main(String[] args) throws IOException, JSONException {
        ResponseParser responseParser = ResponseParser.getInstance();
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        while (!quit) {
            if (KEY == null) {
                System.out.println("Podaj Twoj API Key: ");
                KEY = input.next();
            }


            System.out.println("Dla jakiej lokalizacji sprawdzic pogode: ");
            String localization = input.next();

            System.out.println("Dla ilu dni: ");
            String days = input.next();

            Url url = Url.urlBuilder()
                    .key(KEY)
                    .location(localization)
                    .days(days)
                    .build();

            responseParser.getWeather(url.getUrlAddress());
            url = null;
            System.gc();

            System.out.println("Wyjść? T/N: ");
            quit = input.next().equals("t");
            System.out.flush();
        }
    }
}
