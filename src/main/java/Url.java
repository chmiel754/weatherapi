import java.net.MalformedURLException;
import java.net.URL;


class Url {
    private String key;
    private String location;
    private String days;

    private Url() {
    }

    public static final class UrlBuilder {
        String key;
        String localization;
        String days;

        UrlBuilder key(String key) {
            this.key = key;
            return this;
        }


        UrlBuilder location(String localization) {
            this.localization = localization;
            return this;
        }


        UrlBuilder days(String days) {
            this.days = days;
            return this;
        }

        Url build() {
            if (key.isEmpty()) {
                throw new IllegalStateException("Klucz nie może byc pusty");
            }
            if (localization.isEmpty()) {
                throw new IllegalStateException("Lokalizacja nie może byc pusta");
            }

            Url url = new Url();
            url.key = this.key;
            url.location = this.localization;
            url.days = this.days;
            return url;
        }
    }

    static UrlBuilder urlBuilder() {
        return new UrlBuilder();
    }

    URL getUrlAddress() throws MalformedURLException {
        String url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key="
                + this.key + "&q=" + this.location + "&format=json&num_of_days=" + this.days;
        return new URL(url);
    }
}
