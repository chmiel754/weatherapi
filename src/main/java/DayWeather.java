public class DayWeather {
    private String date;
    private String location;
    private String temp;
    private String windSpeed;
    private String pressure;
    private String sunrise;
    private String sunset;
    private String chanceOfRain;

    public DayWeather(Object location, Object date, Object avgtempC, Object windSpeed, Object pressure, Object sunrise, Object sunset, Object chanceOfRain) {
        this.location = location.toString();
        this.date = date.toString();
        this.temp = avgtempC.toString();
        this.windSpeed = windSpeed.toString();
        this.pressure = pressure.toString();
        this.sunrise = sunrise.toString();
        this.sunset = sunset.toString();
        this.chanceOfRain = chanceOfRain.toString();
    }


    @Override
    public String toString() {
        return String.format("Data: %1$s\nLokalizacja: %2$s\nTemperatura: %3$s\nPrędkość wiatru: %4$s\nCisnienie: %5$s\n"
                        + "Wschód słońca: %6$s\nZachód słońca: %7$s\nPrawdopodobieństwo opadów: %8$s\n",
                this.date, this.location, this.temp + "C", this.windSpeed + "km/h", this.pressure + "hPa",
                this.sunrise, this.sunset, this.chanceOfRain + "%");
    }
}
