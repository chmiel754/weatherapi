import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class ResponseParser {
    private static ResponseParser INSTANCE;

    private ResponseParser() {
    }

    static ResponseParser getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ResponseParser();
        }
        return INSTANCE;
    }

    void getWeather(URL url) throws IOException, JSONException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (!checkConnection(connection)) {
            System.out.println("Brak połączenia z baza danych!");
            Main.KEY = null;
            return;
        } else {
            for (DayWeather day : dataParser(connection)) {
                System.out.println(day);
            }
        }
    }

    private List<DayWeather> dataParser(HttpURLConnection connection) throws IOException, JSONException {
        JSONObject jsonResponse = new JSONObject(responseToString(connection));
        List<DayWeather> dayWeatherList = new ArrayList<>();
        JSONArray weatherArray = jsonResponse.getJSONObject("data").getJSONArray("weather");
        for (int i = 0; i < weatherArray.length(); i++) {
            dayWeatherList.add(new DayWeather(
                    jsonResponse.getJSONObject("data").getJSONArray("request").getJSONObject(0).get("query"),
                    weatherArray.getJSONObject(i).get("date"),
                    weatherArray.getJSONObject(i).get("avgtempC"),
                    weatherArray.getJSONObject(i).getJSONArray("hourly").getJSONObject(0).get("windspeedKmph"),
                    weatherArray.getJSONObject(i).getJSONArray("hourly").getJSONObject(0).get("pressure"),
                    weatherArray.getJSONObject(i).getJSONArray("astronomy").getJSONObject(0).get("sunrise"),
                    weatherArray.getJSONObject(i).getJSONArray("astronomy").getJSONObject(0).get("sunset"),
                    weatherArray.getJSONObject(i).getJSONArray("hourly").getJSONObject(0).get("chanceofrain")));
        }
        return dayWeatherList;
    }

    private String responseToString(HttpURLConnection connection) throws IOException {
        String inputLine;
        BufferedReader in = new BufferedReader(new InputStreamReader((connection.getInputStream())));
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private boolean checkConnection(HttpURLConnection connection) throws IOException {
        return connection.getResponseCode() == 200;
    }
}
